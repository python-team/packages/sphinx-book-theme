#! /usr/bin/make -f
export PYBUILD_NAME=sphinx-book-theme
export MPLCONFIGDIR=/var/tmp
export PYBUILD_BEFORE_TEST=cp -r src/sphinx_book_theme {build_dir}
export PYBUILD_TEST_PYTEST=1
# These tests expect pydata-sphinx-theme==0.15.4, but we have a newer version.
export PYBUILD_TEST_ARGS=-v -k 'not test_build_book and not test_header_repository_buttons and not test_repo_custombranch' {dir}/tests/

pydata_static=/usr/lib/python3/dist-packages/pydata_sphinx_theme/theme/pydata_sphinx_theme/static
theme_static=/usr/lib/python3/dist-packages/sphinx_book_theme/theme/sphinx_book_theme/static
docs_static=$(CURDIR)/debian/sphinx-book-theme-doc/usr/share/doc/sphinx-book-theme-doc/html/_static

%:
	dh $@ --buildsystem=pybuild

execute_after_dh_auto_build:
	mkdir src/sphinx_book_theme/assets/styles/css
	rsass src/sphinx_book_theme/assets/styles/index.scss > src/sphinx_book_theme/assets/styles/css/index.css
	webpack
	python3 ./src/sphinx_book_theme/_compile_translations.py

execute_after_dh_python3:
	cp -a src/sphinx_book_theme/theme debian/python3-sphinx-book-theme/usr/lib/python3/dist-packages/sphinx_book_theme/
	rm -f debian/python3-sphinx-book-theme/usr/lib/python3/dist-packages/sphinx_book_theme/theme/sphinx_book_theme/static/.gitignore

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS)))
execute_before_dh_installdocs:
	PYTHONPATH=$(CURDIR)/src http_proxy='http://127.0.0.1:9/' python3 -m sphinx -T -d debian/doctrees -N -bhtml docs/ debian/html
	find debian/html/_sources -name '*.ipynb' -delete

# Fixed lintian duplicate-files:
execute_after_dh_sphinxdoc:
	jdupes -rl debian/sphinx-book-theme-doc/usr/

execute_before_dh_link:
	set -eu; cd $(pydata_static); find -type f -or -type l | while read f; do \
		if cmp -s $$f $(docs_static)/$$f; then \
			ln -sfv $(pydata_static)/$$f $(docs_static)/$$f; \
		fi \
	done
	set -eu; cd $(CURDIR)/debian/python3-sphinx-book-theme$(theme_static); find -type f -or -type l | while read f; do \
		if cmp -s $$f $(docs_static)/$$f; then \
			ln -sfv $(theme_static)/$$f $(docs_static)/$$f; \
		fi \
	done

override_dh_compress:
	dh_compress -Xsphinx-book-theme-doc/html
endif
